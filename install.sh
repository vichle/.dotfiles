#!/bin/bash

DOTFILES_DIR=~/.dotfiles

# Install git
xcode-select --install

# Clone repository
if [ ! -d "$DOTFILES_DIR" ]; then
  git clone git@bitbucket.org:vichle/.dotfiles.git $DOTFILES_DIR
else
  git --git-dir $DOTFILES_DIR/.git pull origin master
fi

# Install Homebrew
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"

# Install Oh-My-ZSH
sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
chsh -s /bin/zsh

# Install nvm
mkdir -p ~/.nvm
sh -c "$(curl -fsSL https://raw.githubusercontent.com/creationix/nvm/v0.34.0/install.sh)"

# Create .secrets file
touch ~/.secrets/init.sh

# Link config files
ln -svf $DOTFILES_DIR/.zshrc ~/.zshrc
ln -svf $DOTFILES_DIR/.zprofile ~/.zprofile
ln -svf $DOTFILES_DIR/coffeelint.json ~/coffeelint.json
ln -svf $DOTFILES_DIR/.vscode/settings.json ~/Library/Application\ Support/Code/User/settings.json
ln -svf $DOTFILES_DIR/.vscode/launch.json ~/Library/Application\ Support/Code/User/launch.json
ln -svf $DOTFILES_DIR/ssh_config ~/.ssh/config
ln -svf $DOTFILES_DIR/.npmrc ~/.npmrc
ln -svf $DOTFILES_DIR/.gitignore_global ~/.gitignore_global
ln -svf $DOTFILES_DIR/.gitconfig ~/.gitconfig
ln -svf $DOTFILES_DIR/.gitconfig.instabox ~/.gitconfig.instabox
ln -svf $DOTFILES_DIR/.hyper.js ~/.hyper.jss

touch ~/.ssh/id_rsa
touch ~/.ssh/id_rsa.pub

# Install powerlevel 9k
git clone https://github.com/bhilburn/powerlevel9k.git ~/.oh-my-zsh/custom/themes/powerlevel9k
git clone https://github.com/powerline/fonts.git ~/temp/powerline-fonts
~/temp/powerline-fonts/install.sh
rm -rf ~/temp/powerline-fonts

# Install FiraCode fonts
brew tap caskroom/fonts
brew cask install font-fira-code
brew cask install font-firacode-nerd-font


# 1Password CLI
brew cask install 1password-cli

# Install named Home Brew packages
brew install git-flow hyper

# Install App Store CLI
brew install mas

# Install apps from App Store
mas install 803453959 # Slack
mas install 918207447 # Annotate
mas install 1278508951 # Trello

# Download necessary app-files
mkdir -p ~/temp/install
curl -L https://update.code.visualstudio.com/1.31.1/darwin/stable --output ~/temp/install/code.zip
open ~/temp/install/code.zip
curl -L https://app-updates.agilebits.com/download/OPM7 --output ~/temp/install/1password.pkg
curl -L https://download.scdn.co/SpotifyInstaller.zip --output ~/temp/install/spotify.zip
open ~/temp/install/spotify.zip
curl -L https://s3.amazonaws.com/spectacle/downloads/Spectacle+1.2.zip --output ~/temp/install/spectacle.zip
open ~/temp/install/spectacle.zip

# Open folder
open ~/temp/install

read -p "Install VSCode, Chrome and 1Password. Then press any key."
read -p "Install your id_rsa keys, then press any key."

# Add SSH key to keychain
ssh-add
