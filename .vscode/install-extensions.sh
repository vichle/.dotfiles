#!/bin/zsh

code --install-extension pkief.material-icon-theme
code --install-extension wesbos.theme-cobalt2
code --install-extension fractalbrew.backticks
code --install-extension mrmlnc.vscode-duplicate
