# Path to your oh-my-zsh installation.
export TERM="xterm-256color"
export ZSH="/Users/vichle/.oh-my-zsh"
export DOTFILES_DIR=$HOME/.dotfiles

autoload -U +X bashcompinit && bashcompinit

. ~/.secrets/init.sh
. $DOTFILES_DIR/.env
. $DOTFILES_DIR/.alias
. $DOTFILES_DIR/.functions


plugins=(
  git
  git-flow
  coffee
  node
  npm
  yarn
  osx
)
source $ZSH/oh-my-zsh.sh

# Set zsh opts that may have been already set my oh-my-zsh
. $DOTFILES_DIR/.opts

# Set zsh styles
zstyle ':completion:*' accept-exact '*(N)'

zstyle ':completion:*' use-cache yes
zstyle ':completion::complete:*' cache-path ~/


[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion

export PATH=$PATH:$DOTFILES_DIR/scripts
export PATH=$PATH:/usr/local/opt/mongodb-community@4.2/bin
export PATH=$PATH:/Applications/Visual\ Studio\ Code.app/Contents/Resources/app/bin
export PATH=$PATH:$HOME/.cargo/bin
export PATH=$PATH:$(go env GOPATH)/bin

# Always show pwd as tab name
if [ $ITERM_SESSION_ID ]; then
  DISABLE_AUTO_TITLE="true"
  echo -ne "\033];${PWD##*/}\007"
fi

precmd() {
  echo -ne "\033];${PWD##*/}\007"
}

# Loads nvmrc on cd
cd() { builtin cd "$@"; 'load-nvmrc'; }

test -e "${HOME}/.iterm2_shell_integration.zsh" && source "${HOME}/.iterm2_shell_integration.zsh"

# tabtab source for serverless package
# uninstall by removing these lines or running `tabtab uninstall serverless`
[[ -f /Users/vichle/.nvm/versions/node/v8.15.1/lib/node_modules/serverless/node_modules/tabtab/.completions/serverless.zsh ]] && . /Users/vichle/.nvm/versions/node/v8.15.1/lib/node_modules/serverless/node_modules/tabtab/.completions/serverless.zsh
# tabtab source for sls package
# uninstall by removing these lines or running `tabtab uninstall sls`
[[ -f /Users/vichle/.nvm/versions/node/v8.15.1/lib/node_modules/serverless/node_modules/tabtab/.completions/sls.zsh ]] && . /Users/vichle/.nvm/versions/node/v8.15.1/lib/node_modules/serverless/node_modules/tabtab/.completions/sls.zsh
# tabtab source for slss package
# uninstall by removing these lines or running `tabtab uninstall slss`
[[ -f /Users/vichle/.nvm/versions/node/v8.15.1/lib/node_modules/serverless/node_modules/tabtab/.completions/slss.zsh ]] && . /Users/vichle/.nvm/versions/node/v8.15.1/lib/node_modules/serverless/node_modules/tabtab/.completions/slss.zsh

source $DOTFILES_DIR/__doctl.sh

# The next line updates PATH for the Google Cloud SDK.
if [ -f '/Users/vichle/google-cloud-sdk/path.zsh.inc' ]; then . '/Users/vichle/google-cloud-sdk/path.zsh.inc'; fi

# The next line enables shell command completion for gcloud.
if [ -f '/Users/vichle/google-cloud-sdk/completion.zsh.inc' ]; then . '/Users/vichle/google-cloud-sdk/completion.zsh.inc'; fi

function kc() {
    kubectl config use-context $1
}
function _kc() {
    local cur kubectl_out
    cur=${COMP_WORDS[COMP_CWORD]}
    if kubectl_out=$(kubectl config get-contexts -o name 2>/dev/null); then
        COMPREPLY=( $( compgen -W "${kubectl_out[*]}" -- "$cur" ) )
    fi
}
complete -F _kc kc

source $DOTFILES_DIR/kubectl-completion.sh
